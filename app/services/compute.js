'use strict';

angular.module('app')
    .service('compute', function() {
        const $ctrl = this;
        $ctrl.database = {};
        const hotspot = [
          {
            id: 1,
            appearances: 2,
            totalDays: 3,
            probability: 1,
            lat:          45,
            long:         25
          },
          {
            id: 1,
            appearances: 5,
            totalDays: 34,
            probability: 5,
            lat:          46,
            long:         26
          },
          {
            id: 1,
            appearances: 21,
            totalDays: 34,
            probability: 80,
            lat:          47,
            long:         25
          }
        ]

        $ctrl.computeHotspot = (data) => {
            let hotspotDay = data.day;

            $ctrl.database.hotspotDay.forEach((day) => {
                day.hotspot.forEach((hotspot) => {
                    if($ctrl.overlaps(hotspot, data)) {
                        $ctrl.increase(hotspot);
                        //hotspot.increased = true;
                    }

                });
                // if hotspot was not increased today it means the probability must be decreased
                day.hotspot.forEach((hotspot) => {
                    if(!hotspot.increased)
                        $ctrl.decrease(hotspot);
                });
            });
        };
        $ctrl.recalcProbability = (hotspot) => {
            hotspot.probability = hotspot.appearances / hotspot.totalDays;
            console.log(hotspot);
        };

        $ctrl.increase = (hotspot) => {
          hotspot.appearances ++;
          hotspot.totalDays ++;
          $ctrl.recalcProbability(hotspot);
        };

        $ctrl.decrease = (hotspot) => {
          hotspot.totalDays ++;
          $ctrl.recalcProbability(hotspot);
        };

        $ctrl.get = () => {
          return hotspot;
        }

        $ctrl.test = () => {
            hotspot.forEach((hotspot) => {
                console.log(hotspot);
                console.log("Increasing: ");
                $ctrl.increase(hotspot);
                console.log("Decreasing: ");
                $ctrl.decrease(hotspot);
            });
        }
    });
