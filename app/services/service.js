'use strict';

angular.module('app')
    .service('service', function () {
        const $ctrl = this;
        let sidebarVisible = false;
        let username = 'Alexandru Andronache';
        let userClass = 'SysOp';
        let score = '2000';
        const observers = [];

        $ctrl.registerObserver = (observer) => {
          observers.push(observer);
        }

        $ctrl.notifyObservers = () => {
          observers.forEach((observer) => {
              observer.getInfo();
          });
        };

        $ctrl.sidebarShowHide = function () {
            if(sidebarVisible)
                sidebarVisible = false;
            else sidebarVisible = true;
            $ctrl.notifyObservers();
            return sidebarVisible;
        };

        $ctrl.getInfo = function () {
            let info = {
                username,
                userClass,
                score,
                sidebarVisible
            };
            return info;
        };
    });
