'use strict';

angular.module('navbar').component('navbar', {
    templateUrl: 'components/Navbar/navbar.template.html',
    controller: ['service', 'compute',
        function NavbarController(service, compute) {
            const $ctrl = this;

            $ctrl.sidebarShowHide = function () {
                $ctrl.sidebarVisible = service.sidebarShowHide();
            };
        }
    ]
});
