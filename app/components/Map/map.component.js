'use strict';

var hotspots = [];
const oldHotspots = [
  {
      "id":           1,
      "lat":          45,
      "long":         25,
      "year":         2017,
      "month":        10,
      "day":          7,
      "startHour":    20,
      "startMinutes": 15,
      "endHour":      21,
      "endMinutes":   20,
      "probability":  100,
      "description":  "asdasdasdf"
  },
  {
      "id":           2,
      "lat":          46,
      "long":         26,
      "year":         2017,
      "month":        10,
      "day":          7,
      "startHour":    20,
      "startMinutes": 15,
      "endHour":      21,
      "endMinutes":   20,
      "probability":  20
  },
  {
      "id":           3,
      "lat":          47,
      "long":         25,
      "year":         2017,
      "month":        10,
      "day":          7,
      "startHour":    20,
      "startMinutes": 15,
      "endHour":      21,
      "endMinutes":   20,
      "probability":  30
  },
  {
      "id":           4,
      "lat":          48,
      "long":         24,
      "year":         2017,
      "month":        10,
      "day":          7,
      "startHour":    20,
      "startMinutes": 15,
      "endHour":      21,
      "endMinutes":   20,
      "probability":  60
  },
  {
      "id":           5,
      "lat":          47,
      "long":         26,
      "year":         2017,
      "month":        10,
      "day":          7,
      "startHour":    20,
      "startMinutes": 15,
      "endHour":      21,
      "endMinutes":   20,
      "probability":  100
  },
  {
      "id":           6,
      "lat":          44,
      "long":         27,
      "year":         2017,
      "month":        10,
      "day":          7,
      "startHour":    20,
      "startMinutes": 15,
      "endHour":      21,
      "endMinutes":   20,
      "probability":  100
  }
];

angular.module('map').component('map', {
    templateUrl: 'components/Map/map.template.html',
    controller:  ['compute',
        function MapController(compute) {

            const $ctrl               = this;
            const infoWindow          = new google.maps.InfoWindow();
            const mapOptions          = {
                zoom:      6,
                center:    new google.maps.LatLng(45, 25),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            $ctrl.map                 = new google.maps.Map(document.getElementById('map'), mapOptions);
            $ctrl.markers             = [];
            $ctrl.unauthorizedMarkers = [];
            $ctrl.addHotspotListener  = false;
            $ctrl.addHotspotClass     = false;
            $ctrl.infoWindowHover     = false;
//------------------------------------------------------- AICILEA
            hotspots = compute.get();

            $ctrl.Circle = new google.maps.Circle({
                strokeColor:   'black',
                strokeOpacity: 0.08,
                strokeWeight:  20,
                fillColor:     '#add8e6',
                fillOpacity:   0.40,
                map:           $ctrl.map,
                radius:        100000,
                visible:       false,
                flag:          false
            });

            // ADD HOTSPOT
            $ctrl.addHotspot = () => {
                $ctrl.addHotspotListener = true;
                $ctrl.addHotspotClass    = true;

                if ($ctrl.Circle.visible === true) {
                    $ctrl.Circle.setVisible(false);
                    $ctrl.Circle.flag = true;
                }
                $ctrl.markers.forEach(function (marker) {
                    marker.setVisible(false);
                });
            };

            // MAP CLICK LISTENER
            google.maps.event.addListener($ctrl.map, 'click', function (e) {

                let long = parseFloat(e.latLng.lng());
                let lat = parseFloat(e.latLng.lat());
                let myLatLng = new google.maps.LatLng(lat, long);

                infoWindow.close();
                $ctrl.infoWindowHover = false;

                if ($ctrl.addHotspotListener) {
                    let info = {
                      lat,
                      long
                    };
                    const today       = new Date();
                    info.year         = today.getFullYear();
                    info.month        = today.getMonth();
                    info.day          = today.getDate();
                    info.startHour    = today.getHours();
                    info.startMinutes = today.getMinutes();

                    $ctrl.createMarker(info, 'unauth');

                    $ctrl.addHotspotClass = false;
                    $ctrl.addHotspotListener = false;

                    if ($ctrl.Circle.flag === true) {
                        $ctrl.Circle.setVisible(true);
                        $ctrl.Circle.flag = false;
                        $ctrl.markers.forEach(function (marker) {
                            if ($ctrl.Circle.getBounds().contains(marker.getPosition())) {
                                marker.setVisible(true);
                            }
                            else marker.setVisible(false);
                        });
                    }

                    return;
                }
                $ctrl.Circle.setCenter(myLatLng);
                $ctrl.Circle.setVisible(true);

                $ctrl.markers.forEach(function (marker) {
                    if ($ctrl.Circle.getBounds().contains(marker.getPosition())) {
                        marker.setVisible(true);
                    }
                    else marker.setVisible(false);
                });
            });

            // CREATE MARKER
            $ctrl.createMarker = (info, type) => {
                let myLatLng = new google.maps.LatLng(parseFloat(info.lat), parseFloat(info.long));
                let icon   = {
                    url:        'http://icons.iconarchive.com/icons/martz90/hex/512/warning-icon.png',
                    scaledSize: new google.maps.Size(50, 50)
                };
                let marker = new google.maps.Marker({
                    map:      $ctrl.map,
                    position: myLatLng,
                    icon:     icon,
                    title:    '',
                    visible:  false,
                    content:  '<div class="infoWindowDescription"><div class="infoWindowProbability">Probability: <span class="infoWindowProbabilityNo"><b>' + info.probability + '%</b></span><p class="infoWindowPretext">You may encounter difficulties if you choose to go this way.</p></div><div class="feedbackContainer centered"><div class="rosu"><span class="glyphicon glyphicon-remove"></span></div><div class="verde"><span class="glyphicon glyphicon-ok"></span></div></div></div>'
                });

                if (type === 'default') {
                    google.maps.event.addListener(marker, 'mouseover', function () {
                        infoWindow.setContent(marker.content);
                        infoWindow.open($ctrl.map, marker);
                    });
                    $ctrl.markers.push(marker);
                }
                else if (type === 'unauth') {
                    icon.url = 'https://png.icons8.com/ask-question/color/1600';
                    marker.setIcon(icon);
                    marker.setVisible(true);

                    google.maps.event.addListener(marker, 'mouseover', function () {
                        infoWindow.setContent('<div class="infoWindowDescription"><p class="thankYou">Thank you!</p><p>Your notification has been sent to us. We will do our best to provide you with up to date statistics.</p></div>');
                        infoWindow.open($ctrl.map, marker);
                    });

                    infoWindow.setContent('<div class="infoWindowDescription"><p class="thankYou">Thank you!</p><p>Your notification has been sent to us. We will do our best to provide you with up to date statistics.</p></div>');
                    infoWindow.open($ctrl.map, marker);
                    setTimeout(function () {
                        infoWindow.close();
                    }, 3000);
                    $ctrl.unauthorizedMarkers.push(marker);
                }

                google.maps.event.addListener(marker, 'mouseout', function () {
                  if(!$ctrl.infoWindowHover)
                    infoWindow.close();

                  $ctrl.infoWindowHover = false;
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.open($ctrl.map, marker);
                    compute.increase(marker);
                    hotspots = compute.get();
                    $ctrl.load();

                    $ctrl.infoWindowHover = true;
                });
            };

            $ctrl.feedback = (type) => {
              console.log("yee");
                if(type){
                  console.log("thanks for your UPVOTE!");

                }
                else {
                  console.log("**** you for your DOWNVOTE!");
                }
            };

            // LOAD MARKERS
            $ctrl.load = () => {
                hotspots.forEach(function (marker) {
                    $ctrl.createMarker(marker, 'default');
                });
            }

            $ctrl.load();

        }
    ]
});
