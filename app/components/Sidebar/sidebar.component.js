'use strict';

angular.module('sidebar').component('sidebar', {
    templateUrl: 'components/Sidebar/sidebar.template.html',
    controller: ['service',
        function SidebarController(service) {
            const $ctrl = this;
            $ctrl.info = service.getInfo();
            $ctrl.getInfo = () => {
              $ctrl.info = service.getInfo();
            };
            service.registerObserver($ctrl);
        }
    ]
});
