'use strict';

angular.module('app', [
    'ngRoute',
    'map',
    'navbar'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.when('/', {
        template: '<map></map>'
    }).otherwise({redirectTo: '/'});
}]);
